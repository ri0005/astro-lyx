How to write astrophysics papers with LyX  
=========================================

You have to install some files into your personal *LyX* directory. On *UNIX* this is in `$HOME/.lyx` but on your system it may be elsewhere.

1.  Clone this repository, e.g.

        git clone gitlab@gitlab.eps.surrey.ac.uk:ri0005/astro-lyx.git

2.  Go into the `astro-lyx` directory.

3.  Copy the contents of `bind/` to `$HOME/.lyx/bind/`

4.  Copy the contents of `layouts/` to `$HOME/.lyx/layouts/`

5.  Copy the contents of `macros/` to `$HOME/.lyx/layouts/`

6.  In *LyX* go to Tools &rarr; Reconfigure

7.  Restart *LyX*

Monthly Notices of the Royal Astronomical Society (MNRAS) 
---------------------------------------------------------

1.  Install the *MNRAS* LaTeX style files on your system (e.g. in `$HOME/.latex/`)\
    General instructions are at <https://academic.oup.com/mnras/pages/General_Instructions>\
    The *LaTeX* package is at <https://ctan.org/pkg/mnras?lang=en>

2.  Open *LyX*, go to Tools &rarr; Reconfigure, restart *LyX* (you only have to do this when installing new files)

3.  Load the file `MNRAS_paper.lyx`

4.  Change the text to include your writing

Note: I have called the *MNRAS* style file `mnras3.cls` because I already had a previous (V2) version called mnras.cls. If you only have `mnras.cls` from the newest V3 package, try running (e.g. in `$HOME/.latex/`)

    ln -s mnras.cls mnras3.cls

Astronomy and Astrophysics (A&A) 
--------------------------------

1.  Install the *A&A* LaTeX package from <https://www.aanda.org/for-authors>

2.  Open *LyX*, go to Tools &rarr; Reconfigure, restart *LyX* (you only have to do this when installing new files)

3.  Load the file `AA_paper.lyx`

4.  Change the text to include your writing

Note: *LyX* comes with an *aa.layout* file, which I use in the above, if this doesn't work for you let me know (I have a customised version with more features you can try).

See also <https://wiki.lyx.org/Layouts/Astronomy-Astrophysics>.

Galaxies 
--------

To follow!
