# This file is part of
# ==================================================================
#
#                  LyX, the High Level Word Processor
# 
#         Copyright (C) 1995-1997 Asger Alstrup & The LyX Team
#
# ==================================================================

#
# This is the CUA (Common User Access) flavour bind file, based on 
# bindings found in the Windows, Mac and Motif world.
#
# This is Rob's version.
############################################################

# Free bindings:

#\bind "C-h"
#\bind "C-j"
#\bind "C-y"



# include one of the basic flavours (cua or emacs)
\bind_file	"cua"

### Rob's bindings
### M_sun
\bind "M-q"		"command-sequence  font-roman; math-insert \,;  math-insert M; math-subscript; math-insert \odot;"

### R_sun

\bind "M-a"		"command-sequence font-roman; math-insert \,;  math-insert R; math-subscript; math-insert \odot; "

### L_sun

\bind "M-z"		"command-sequence  font-roman; math-insert \,; math-insert L; math-subscript; math-insert \odot;"

### [X/Fe]
\bind "M-S-f" "command-sequence  font-roman; math-insert \left[\mathrm{}/\mathrm{Fe}\right];"

### [X/H]
\bind "M-S-h" "command-sequence  font-roman; math-insert \left[\mathrm{}/\mathrm{H}\right];"

### yr^-1
\bind "M-y"		"command-sequence font-roman; math-insert \,; font-roman; math-insert y; font-roman; math-insert r; math-superscript; math-insert -; math-insert 1;"

### \theequation
\bind "M-t"             "command-sequence math-insert \theequation;"

\bind "M-r"             "command-sequence math-insert \mathrm{};"

### euro
\bind "C-S-e"		 "command-sequence ert-insert; self-insert \euro{}; inset-toggle"

# some emacs-bindings
\bind "C-b"			"char-backward"
\bind "C-k"                    "line-delete-forward"
#\bind "C-a"			"line-begin"
#\bind "C-f"			"char-forward"
# Should have been "buffer-write-some"
\bind "C-x s"                  "buffer-write"
\bind "C-x C-f"                "file-open"
\bind "C-BackSpace"		"word-delete-backward"

# bold font
\bind		"C-b" "font-bold"

\bind "C-a" "command-sequence math-insert \answer;"

# <> are left and right arrows
\bind "M-S-greater" "math-insert \\rightarrow"
\bind "M-S-less" "math-insert \\leftarrow"



# include math bindings
\bind_file      "math"


############################################################
# bind ctrl-tab and shift-ctrl-tab to buffer next and previous
\bind "C-Tab" "buffer-next"
\bind "C-S-Backtab" "buffer-previous"
